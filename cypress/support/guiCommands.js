import 'cypress-xpath';
/// <reference types="Cypress" />


Cypress.Commands.add('login', () => {
  // cy.visit('users/sign_in')
  cy.visit('/users/sign_in')
  cy.xpath("//input[@id='user_login']").type(Cypress.env('user_name'));
  cy.xpath("//input[@id='user_password']").type(Cypress.env('user_password'));
  cy.xpath("//input[@class='btn btn-success']").click();
});

Cypress.Commands.add('logout', () => {
  cy.get(".header-user-dropdown-toggle").click();
  cy.xpath("//a[.='Sign out']").click();
  // cy.contains('Sign out').click();
});

Cypress.Commands.add('gui_CreateProject', ({name, description}) => {
  cy.visit("/projects/new");
  cy.xpath("//div[@id='blank-project-name']//input[@id='project_name']").type(name);
  cy.xpath("//div[@id='blank-project-pane']//textarea[@id='project_description']").type(description);
  cy.xpath("//div[@id='blank-project-pane']//input[@value='20']").check();
  cy.xpath("//input[@id='project_initialize_with_readme']").click();
  cy.contains("Create project").click();
});

Cypress.Commands.add('gui_createIssue', ({title, description, project}) => {
  cy.visit(`${Cypress.env('user_name')}/${project.name}/issues/new`);
  cy.xpath("//input[@id='issue_title']").type(title);
  cy.xpath("//textarea[@id='issue_description']").type(description);
  cy.xpath("//input[@name='commit']").click()
});

Cypress.Commands.add('gui_SetLabelOnIssue', ({name, color}) => {
  cy.xpath("//div[@class='block labels']//a[.='Edit']").click();
  cy.contains(name).click();
  cy.xpath("//div[@class='content-wrapper']").click();
});

Cypress.Commands.add('gui_SetMilestoneOnIssue', ({title}) => {
  cy.xpath("//div[@class='block milestone']//a[.='Edit']").click();
  cy.contains(title).click();
});