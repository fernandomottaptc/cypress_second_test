/// <reference types="Cypress" />

Cypress.Commands.add('cli_CloneWithSSH', ({name}) => { 
  const domain = Cypress.config('baseUrl').replace('http://', '').replace('/', '');
  cy.exec(`cd temp/ && git clone git@${domain}:${Cypress.env('user_name')}/${name}.git`);
})
