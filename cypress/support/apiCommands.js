/// <reference types="Cypress" />

const accessToken = Cypress.env('gitlab_access_token');

Cypress.Commands.add('api_CreateProject', ({name, description}) => {
  cy.request({
    method: 'POST',
    url: `api/v4/projects/?private_token=${accessToken}`,
    body: {
      name,
      description,
      initialize_with_readme: true
    }
  });
});

Cypress.Commands.add('api_CreateIssue', ({title, description, project}) => {
  cy.api_CreateProject(project).then(response => {
    cy.request({
      method: 'POST',
      url: `api/v4/projects/${response.body.id}/issues?private_token=${accessToken}`,
      body: {
        title,
        description,
        confidential: false
      }
    });
  });
});

Cypress.Commands.add('api_CreateLabel', (projectId ,{name, color}) => {
  cy.request({
    method: 'POST',
    url: `api/v4/projects/${projectId}/labels?private_token=${accessToken}`,
    body: {
      name,
      color
    }
  });
});

Cypress.Commands.add('api_CreateMilestone', (projectId, {title}) => {
  cy.request({
    method: 'POST',
    url: `/api/v4/projects/${projectId}/milestones?private_token=${accessToken}`,
    body: {title}
  })
});