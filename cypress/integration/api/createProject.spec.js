import * as fake from 'faker';

/// <reference types="Cypress" />

describe('Should be create a new Project using api:', () => {
  it('successfully', () => {
    const project = {
      name: `project-${fake.random.uuid()}`,
      description: fake.random.words(10)
    }

    cy.api_CreateProject(project).then(response => {
      // console.log(response.status);
       expect(response.status).to.equal(201);
       expect(response.body.name).to.equal(project.name);
       expect(response.body.description).to.equal(project.description);
    });
    
  });
});