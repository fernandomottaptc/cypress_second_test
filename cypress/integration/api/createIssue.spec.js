import * as faker from 'faker';

/// <reference types="Cypress" />

describe('Script should be able create a new issue at created project.', () =>{

  const issue = {
    title: `issue-${faker.random.uuid()}`,
    description: faker.random.words(10),
    project: {
      name: `project-${faker.random.uuid()}`,
      description: faker.random.words(5),
    }
  }

  it('After create a new issue, must return a sucessuful message', () => {
    cy.api_CreateIssue(issue).then(response => {
      expect(response.status).to.equal(201);
      expect(response.body.title).to.equal(issue.title);
      expect(response.body.description).to.equal(issue.description);
    });
  });
});
