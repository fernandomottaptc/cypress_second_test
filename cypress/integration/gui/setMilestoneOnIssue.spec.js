import { random } from 'faker';

/// <reference types="Cypress" />

describe('Should be able to create a milestone on the issue', () => {
  const issue = {
    title: `issue-${random.uuid()}`,
    description: random.words(3),
    project: {
      name: `project-${random.uuid()}`,
      description: random.words(5)
    }
  }

  const milestone = {
    title: `milestone-${random.word()}`
  }

  beforeEach(() => {
    cy.login();
    cy.api_CreateIssue(issue).then(response => {
      cy.api_CreateMilestone(response.body.project_id, milestone)
      cy.visit(`${Cypress.env('user_name')}/${issue.project.name}/issues/${response.body.iid}`);
    })
  });

  it('Should be return a sucessiful status. After verification of milestone data.', () => {
    cy.gui_SetMilestoneOnIssue(milestone);
    cy.get(".milestone").should('contain', milestone.title);
  })
});