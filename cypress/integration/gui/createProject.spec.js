import * as faker from 'faker';

/// <reference types="Cypress" />

describe('Script should be able to create a new project on Gitlab.', _ => {
  before(() => cy.login());

  it('Create a project', () => {
    const project = {
      name: `project-${faker.random.uuid()}`,
      description: faker.random.words(20)
    }

    cy.gui_CreateProject(project);

    cy.url().should('be.equal', `${Cypress.config('baseUrl')}${Cypress.env('user_name')}/${project.name}`);
    cy.contains(project.name).should('be.visible');
    cy.contains(project.description).should('be.visible');
  });

});