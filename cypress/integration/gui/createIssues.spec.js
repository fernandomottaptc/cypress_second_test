import * as faker from 'faker';

/// <reference types="Cypress" />

describe('Script should be able create a new issue at created project.', () =>{

  const issue = {
    title: `issue-${faker.random.uuid()}`,
    description: faker.random.words(10),
    project: {
      name: `project-${faker.random.uuid()}`,
      description: faker.random.words(5),
    }
  }

  before(() => {
    cy.login();
    cy.gui_CreateProject(issue.project);
  });

  it('After create a new issue must return a sucessuful message', () => {
    cy.gui_createIssue(issue);

    cy.xpath("//div[@class='issue-details issuable-details']")
    .should('contain', issue.title)
    .and('contain', issue.description);
  })
});

//strong[@class='dropdown-menu-user-full-name']