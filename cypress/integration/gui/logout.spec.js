/// <reference types="Cypress" />

describe('Logout', () => {
  beforeEach(() => cy.login())

  it('Should be able logout of application', () => {
    cy.logout();

    cy.url().should('be.equal', `${Cypress.config('baseUrl')}users/sign_in`);
  })
})