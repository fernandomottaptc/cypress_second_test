import { random } from 'faker';

/// <reference types="Cypress" />

describe('Be able to set a new issue', () => {
  const issue = {
    title: `issue-${random.uuid()}`,
    description: random.words(3),
    project: {
      name: `project-${random.uuid()}`,
      description: random.words(5)
    }
  }
  const label = {
    name: `label-${random.word()}`,
    color: '#ffaabb'
  }

  beforeEach(() => {
    cy.login();
    cy.api_CreateIssue(issue)
      .then(response => {
        cy.api_CreateLabel(response.body.project_id, label);
        cy.visit(`${Cypress.env('user_name')}/${issue.project.name}/issues/${response.body.iid}`);
      });
  });

  it('be able to access a new issue page and due a verification', () => {
    cy.gui_SetLabelOnIssue(label)
    console.log(label);
    cy.get(".dont-hide").should('contain', label.name);
    cy.get(".dont-hide span a span").should('have.attr', 'style', `background-color: ${label.color};`);
  })
});