import { random } from 'faker';

/// <reference types="Cypress" />

describe('Should be able to clone a GitLab project and verify key values', () => {
  const project = {
    name: `project-${random.uuid()}`,
    description: random.words(5)
  }

  beforeEach(() => cy.api_CreateProject(project));

  it('should be a succesful when we read project data and confirm key values.', () => {
    cy.cli_CloneWithSSH(project);

    cy.readFile(`temp/${project.name}/README.md`)
      .should('contain', `# ${project.name}`)
      .and('contain', project.description);
  });
});
